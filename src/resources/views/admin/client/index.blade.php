@extends('layouts.admin')

@section('title', 'Clientes')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista de Clientes
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover table-responsive datatable">
                        <thead>
                        <tr>
                            <th>Nome do Cliente</th>
                            <th>Celular</th>
                            <th>Pontos</th>
                            <th>Opções</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($clients as $client)
                            <tr>
                                <td>{{ $client->name }} </td>
                                <td>{{ $client->phone }}</td>
                                <td>{{ $client->points }}</td>
                                <td>
                                    <div class="bottom-actions">
                                        <a href="{{ route('admin.client.manage_sales', $client->id) }}" class="btn btn-primary">Gerenciar Vendas</a>
                                        <a href="{{ route('admin.client.manage_purchases', $client->id) }}" class="btn btn-primary">Gerenciar Compras</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection