<ul class="special-menu pb-4">
    <li>
        <img src="{{ asset('images/produtos/chivito.png') }}" class="img-fluid" alt="">
        <h3>Chivito</h3>
        <p><span>Um sanduíche clássico das ruas uruguaias</span></p>
        <strong class="special-menu-price text-color-dark">R$29,90</strong>
    </li>
    <li>
        <img src="{{ asset('images/produtos/choripan.png') }}" class="img-fluid" alt="">
        <h3>Choripan</h3>
        <p><span>Um famoso sanduíche de linguiça toscana argentino</span></p>
        <strong class="special-menu-price text-color-dark">R$21,90</strong>
    </li>
    <li>
        <img src="{{ asset('images/produtos/cubano.png') }}" class="img-fluid" alt="">
        <h3>Cubano</h3>
        <p><span>Seu nome já entrega a sua origem, Cubano é um sanduíche de sobre paleta suína desfiada</span></p>
        <strong class="special-menu-price text-color-dark">R$29,90</strong>
    </li>
    <li>
        <img src="{{ asset('images/produtos/completo.png') }}" class="img-fluid" alt="">
        <h3>Completo</h3>
        <p><span>Clássico cachorro-quente chileno, com mousse de abacate com limão e pico de galo</span></p>
        <strong class="special-menu-price text-color-dark">R$19,90</strong>
    </li>
    <li>
        <img src="{{ asset('images/produtos/maiz.png') }}" class="img-fluid" alt="">
        <h3>Maíz</h3>
        <p><span>Direto do México, milho verde cozido, maionese com um mix de temperos, parmesão e muito mais!</span></p>
        <strong class="special-menu-price text-color-dark">R$9,90</strong>
    </li>
    <li>
        <img src="{{ asset('images/produtos/coxa-creme.png') }}" class="img-fluid" alt="">
        <h3>Coxa Creme</h3>
        <p><span>Dos subúrbios do Brasil, deliciosa coxa de frango assada, empanada numa massa cremosa e frita!</span></p>
        <strong class="special-menu-price text-color-dark">R$13,90</strong>
    </li>
    <li>
        <img src="{{ asset('images/produtos/ceviche.png') }}" class="img-fluid" alt="">
        <h3>Ceviche</h3>
        <p><span>Cubinhos de peixe branco marinado em suco de limão, típico do Peru. Acompanha pão quentinho.</span></p>
        <strong class="special-menu-price text-color-dark">R$24,90</strong>
    </li>
</ul>
