<?php

namespace App\Http\Controllers\Admin;

use App\UnregisteredSale;
use App\Product;
use App\Http\Requests\UnregisteredSaleRequest;
use App\Http\Controllers\Controller;
use DB;

class UnregisteredSaleController extends Controller
{
    public function index () {
      $contents = UnregisteredSale::all();
      $products = Product::all()->pluck('name', 'id');

      return view('admin.unregistered-sale.index', ['contents' => $contents, 'products' => $products]);
    }

    public function store (UnregisteredSaleRequest $request) {
      $data = $request->all();
      $unregisteredSales = [];

      foreach($data['products'] as $productData) {
        $product = Product::find($productData['product_id']);

        $unregisteredSale = [
          'product_id' => $product->id,
          'credited_points' => $product->points_on_purchase,
          'phone' => $data['phone'],
          'date' => $data['date']
        ];

        for ($i = 0; $i < $productData['quantity']; $i++) {
          array_push($unregisteredSales, $unregisteredSale);
        }
      }

      DB::transaction(function () use ($unregisteredSales) {
        DB::table('unregistered_sales')->insert($unregisteredSales);
      });

      return redirect()->route('admin.unregistered-sale.index');
    }
}
