@extends('layouts.admin')

@section('title', 'Usuários')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if (isset($user))
                        Atualização de dados do Usuário
                    @else
                        Cadastro de Usuário
                    @endif
                </div>
                @if (isset($user))
                    {!! Form::model($user, ['route' => ['admin.user.update', $user->id], 'method' => 'PUT']) !!}
                @else
                    {!! Form::open() !!}
                @endif
                <div class="panel-body">
                  @include('includes/_error_list')
                    <div class="form-group">
                        {!! Form::label('name', 'Nome') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('cpf', 'CPF') !!}
                        {!! Form::text('cpf', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', 'E-mail') !!}
                        {!! Form::email('email', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('password', 'Senha') !!}
                        {!! Form::password('password', ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('password_confirmation', 'Confirmação') !!}
                        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('authority', 'Tipo de Usuário') !!}
                        {!! Form::select('authority', [0 => 'Caixa', 100 => 'Gerente'] , null, [
                            'class' => 'form-control',
                            'placeholder' => 'Selecione o tipo de usuário'
                        ]) !!}
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success">
                                    @if (isset($user))
                                        Atualizar Dados
                                    @else
                                        Cadastrar Usuário
                                    @endif
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
@endsection
