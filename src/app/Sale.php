<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{

    public $timestamps = false;

    protected $fillable = [
      'product_id', 'client_id', 'credited_points', 'debited_points', 'date'
    ];

    protected $dates = [
      'date'
    ];

    public function client()
    {
      return $this->belongsTo(Client::class);
    }

    public function product()
    {
      return $this->belongsTo(Product::class);
    }
}
