@extends('site.layouts.master')

@section('content')
<div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1 class="mt-5 mb-2 pt-3">Nossa <strong>Localização</strong></h1>
                <p class="text-3">Estamos no Food Park Carioca! Consulte o mapa abaixo para verificar a nossa localização! Nossos Horários de Funcionamento:</p> <p>De terça à sexta de 17:00 até 23:45 e</p> <br> sábados e domingos de 15:00 até 23:45.

                <hr class="custom-divider">
            </div>
        </div>
    </div>

<!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
<div id="googlemaps" class="google-map m-0" style="height: 425px;"></div>

<section>
    <div class="container">
        <div class="row justify-content-center text-center pt-4 mt-4 pb-4 mb-4">
            <div class="col-lg-8 pb-3">
                <h2 class="font-weight-normal custom-font-size-1 mb-4">Entre em contato conosco</h2>
                <p>Dúvidas, sugestões, reclamações e qualquer outro assunto!</p>

                {{-- <div class="alert alert-success d-none mt-4" id="contactSuccess">
                    <strong>Success!</strong> Mensagem enviada! Aguarde um contato de retorno!
                </div>

                <div class="alert alert-danger d-none mt-4" id="contactError">
                    <strong>Error!</strong> Há um erro no envio de sua mensagem.
                    <span class="text-1 mt-2 d-block" id="mailErrorMessage"></span>
                </div> --}}

                <form class="form" id="contactForm" action="php/contact-form.php" method="POST" onSubmit="return false;">
                    <div class="form-row">
                        <div class="form-group text-left col">
                            <input type="text" placeholder="Nome" value="" data-msg-required="Informe seu nome" maxlength="100" class="form-control" name="name" id="name" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group text-left col">
                            <input type="email" placeholder="E-mail" value="" data-msg-required="Informe seu e-mail" data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group text-left col">
                            <input type="text" placeholder="Assunto" value="" data-msg-required="Informe o assunto." maxlength="100" class="form-control" name="subject" id="subject" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group text-left col">
                            <textarea maxlength="5000" placeholder="Mensagem" data-msg-required="Digite sua mensagem" rows="10" class="form-control" name="message" id="message" required></textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group text-center col">
                            <input type="submit" value="Enviar" class="btn btn-quaternary  btn-lg text-uppercase font-weight-semibold" data-loading-text="Loading...">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYkqlqJiaqDP_bOPihMm_S8C2iI5dd8_A"></script>
<script>

	/*
	Map Settings

		Find the Latitude and Longitude of your address:
			- http://universimmedia.pagesperso-orange.fr/geo/loc.htm
			- http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

	*/

	// Map Markers
	var mapMarkers = [{
		address: "Rua Mariz e Barros, 1037",
		html: "<strong>La Mezcla</strong><br>Rio de Janeiro, Rua Mariz e Barros, 1037<br><br><a href='#' onclick='mapCenterAt({latitude: -22.91837249, longitude: -43.22192427, zoom: 16}, event)'>[+] zoom</a>",
		icon: {
			image: "img/demos/photography/pin.png",
			iconsize: [31, 41],
			iconanchor: [31, 41]
		}
	}];

	// Map Initial Location
	var initLatitude = -22.91837249;
	var initLongitude = -43.22192427;

	// Map Extended Settings
	var mapSettings = {
		controls: {
			draggable: (($.browser.mobile) ? false : true),
			panControl: true,
			zoomControl: true,
			mapTypeControl: true,
			scaleControl: true,
			streetViewControl: true,
			overviewMapControl: true
		},
		scrollwheel: false,
		markers: mapMarkers,
		latitude: initLatitude,
		longitude: initLongitude,
		zoom: 14
	};

	var map = $('#googlemaps').gMap(mapSettings),
		mapRef = $('#googlemaps').data('gMap.reference');

	// Map text-center At
	var mapCenterAt = function(options, e) {
		e.preventDefault();
		$('#googlemaps').gMap("centerAt", options);
	}

	// Styles from https://snazzymaps.com/
	var styles = [{"featureType":"all","elementType":"geometry.fill","stylers":[{"color":"#c33348"},{"saturation":"-13"},{"lightness":"66"},{"gamma":"2.68"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#484349"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f6efef"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"saturation":"-54"},{"lightness":"-12"}]},{"featureType":"road","elementType":"labels.text","stylers":[{"visibility":"on"},{"hue":"#c33348"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#c33348"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#c33348"},{"visibility":"on"},{"gamma":"3.69"},{"lightness":"61"},{"saturation":"0"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"saturation":"-11"},{"lightness":"-17"},{"gamma":"3.27"}]}];

	var styledMap = new google.maps.StyledMapType(styles, {
		name: 'Styled Map'
	});

	mapRef.mapTypes.set('map_style', styledMap);
	mapRef.setMapTypeId('map_style');

</script>
@endpush
