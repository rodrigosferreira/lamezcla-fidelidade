<?php

namespace App\Http\Controllers\Site;

use App\Http\Requests\ClientRequest;
use App\Http\Controllers\Controller;
use App\Client;
use App\UnregisteredSale;
use App\Sale;
use DB;

class ClientController extends Controller
{
    public function create() {
        return view('site.client.create');
    }

    public function store(ClientRequest $request) {
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $data['points'] = 0;

        $unregisteredSales = UnregisteredSale::where('phone', $data['phone'])->get();

        DB::transaction(function () use ($data, $unregisteredSales) {
            $client = Client::create($data);

            foreach ($unregisteredSales as $sale) {
                $new_sale['client_id'] = $client->id;
                $new_sale['product_id'] = $sale->product_id;
                $new_sale['credited_points'] = $sale->credited_points;
                $new_sale['debited_points'] = 0;
                $new_sale['date'] = $sale->date;
        
                Sale::create($new_sale);
                UnregisteredSale::where('id', $sale->id)->first()->delete();
                $client->update(['points' => $client->points + $sale->credited_points]);
            }
        });

        return redirect()->route('site.client.dashboard');
    }

}
