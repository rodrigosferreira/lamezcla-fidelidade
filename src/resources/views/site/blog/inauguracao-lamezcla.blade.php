@extends('site.layouts.master')

@section('content')
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h1 class="mt-5 mb-2 pt-3">Blog La Mezcla</h1>
							<p class="text-3">Um pouco sobre nós! Esperamos que você possa nos conhecer melhor<br> Traremos um pouco sobre a cultura latino americana e os principais aspectos de nossa cozinha, além de aproximá-los para o nosso dia-a-dia! Sejam bem vindos!</p>

							<hr class="custom-divider">
						</div>
					</div>
				</div>

				<div class="container">

					<div class="row pt-2">
						<div class="col">
							<div class="blog-posts single-post mt-5">

								<article class="post post-large blog-single-post">

									<div class="post-date">
										<span class="day">22</span>
										<span class="month">Abril</span>
									</div>

									<div class="post-content">

										<h1 class="mb-3">Inauguração La Mezcla</h1>

										<div class="post-meta">
											<span><i class="fas fa-user"></i> Por <a href="#">Rodrigo Ferreira</a> </span>
											<span><i class="fas fa-tag"></i> <a href="#">Inauguração</a>, <a href="#">Novidades</a> </span>
										</div>

										<img src="/images/inauguracao.png" class="img-fluid float-right mb-3 mb-1 ml-4" alt="" style="width: 330px;">

										<p class="lead">O dia está próximo.. um projeto a ser inaugurado com meses de suor e dedicação para que tudo saia conforme planejado.</p>

										<p>Uma aposta, com uma pitada de experiência e um conjunto de sabores e bom gosto para trazer um pouco da gastronomia latino americana para nossas vidas.</p>

										<p>Nossa inauguração está chegando! Gostaríamos de convidar a todos os nossos clientes, amigos e família para conhecer o La Mezcla — Cozinha Autoral Latino Americana, que ocorrerá no dia 01º de Maio de 2018 (Dia do Trabalhador, feriado.. coincidência?)</p>

										<p>Esperamos que você possa celebrar conosco este momento especial! Será um prazer recebê-lo em nosso espaço!</p>

										<p>Equipe La Mezcla</p>

										<div class="post-block post-author mt-5 clearfix">
											<h4 class="mt-5 mb-3">Autor</h4>
											<div class="img-thumbnail d-block">
												<a href="#">
													<img src="/images/perfis/profile-rodrigo.jpg" alt="">
												</a>
											</div>
											<p><strong class="name mb-3">Rodrigo Ferreira</strong></p>
											<p class="mt-1"></p>
										</div>
									</div>
								</article>

							</div>
						</div>
					</div>

				</div>
		</div>
@endsection
