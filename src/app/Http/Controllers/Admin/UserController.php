<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index() {
        if (auth('web')->user()->authority < 100) {
            return redirect()->route('admin.dashboard');
        }

        $users = User::all();

        return view('admin.user.index', ['users' => $users]);
    }

    public function create() {
        if (auth('web')->user()->authority < 100) {
            return redirect()->route('admin.dashboard');
        }

        return view('admin.user.form');
    }

    public function store(UserRequest $request) {
        if (auth('web')->user()->authority < 100) {
            return redirect()->route('admin.dashboard');
        }

        $data = $request->all();
        $data['password'] = bcrypt($data['password']);

        User::create($data);
        return redirect()->route('admin.user.index');
    }

    public function edit($id) {
        if (auth('web')->user()->authority < 100) {
            return redirect()->route('admin.dashboard');
        }

        $user = User::findOrFail($id);

        return view('admin.user.form', ['user' => $user]);
    }

    public function update(UserRequest $request, $id) {
        if (auth('web')->user()->authority < 100) {
            return redirect()->route('admin.dashboard');
        }

        $data = $request->all();

        if ($data['password'] == null) {
            unset($data['password']);
        } else {
            $data['password'] = bcrypt($data['password']);
        }

        $user = User::findOrFail($id);

        $user->update($data);

        return redirect()->route('admin.user.index');
    }

    public function destroy($id) {
        if (auth('web')->user()->authority < 100) {
            return redirect()->route('admin.dashboard');
        }

        User::destroy($id);
        return redirect()->route('admin.user.index');
    }
}
