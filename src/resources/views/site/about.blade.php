@extends('site.layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1 class="mt-5 mb-2 pt-3">Sobre <strong>Nós</strong></h1>
              <p class="text-3"></p>

            <hr class="custom-divider">
        </div>
    </div>
</div>

<section class="section section-no-border section-primary custom-section-spacement-1 m-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 mb-4 mb-lg-0">
                <div class="feature-box feature-box-style-2">
                    <div class="feature-box-icon">
                        <i class="fa fa-eye"></i>
                    </div>
                    <div class="feature-box-info">
                        <h2 class="text-uppercase text-color-dark font-weight-bold pt-1 mb-0">Nossa Visão</h2>
                        <p class="custom-text-color-4 text-4 mb-4">Um mar de descobrimentos</p>
                    </div>
                </div>
                <p class="custom-text-color-4">Levar em um futuro não muito distante a gastronomia latino americana para todos os cantos do Brasil. Sermos influenciados pela nossa incrível multicultura e influenciar de forma saborosa e exemplar através de nossa cozinha autoral. </p>
            </div>
            <div class="col-lg-6">
                <div class="feature-box feature-box-style-2">
                    <div class="feature-box-icon">
                        <i class="fa fa-hourglass-half"></i>
                    </div>
                    <div class="feature-box-info">
                        <h2 class="text-uppercase text-color-dark font-weight-bold pt-1 mb-0">Nossa Missão</h2>
                        <p class="custom-text-color-4 text-4 mb-4">Experiências</p>
                    </div>
                </div>
                <p class="custom-text-color-4">Transformar pratos típicos e autorais em sensações inesquecíveis que levem nosso cliente a querer conhecer, vivenciar e experimentar os sabores da América Latina.</p>
            </div>
        </div>
    </div>
</section>

<section class="section section-no-border custom-section-spacement-1 background-color-light m-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 mb-4 mb-lg-0">
                <h2 class="text-uppercase text-color-dark font-weight-bold">Nossa História</h2>
                <p class="text-4 text-color-dark mb-4">Primeiros Passos</p>
                <p class="custom-text-color-1">E foi dada a largada para a grande aventura latino americana! La Mezcla! Como são ricas, surpreendentes e deliciosas as descobertas que estamos fazendo.  Queremos transformar toda essa cultura em experiências únicas,  queremos que você participe disso, que aproveite cada sabor, cada sensação, cada  vivência proporcionada por nós! Contamos com você, com sua curiosidade,  com seu instinto aventureiro, com sua vontade de conhecer mais do que já conhece. Contamos para fazer essa grande aventura do La Mezcla realidade!!
</p>
                <div class="custom-timeline">
                    <div class="custom-timeline-box">
                        <span class="date font-weight-bold text-color-primary">2018</span>
                        <h4 class="text-color-dark font-weight-semibold">Empresa formada!</h4>
                        <p class="custom-text-color-1">Começamos nossas atividades em Março, para que tudo fique pronto para nossa inauguração, em maio, no Food Park Carioca! </p>
                    </div>
                  {{--  <div class="custom-timeline-box">
                        <span class="date font-weight-bold text-color-primary">2005</span>
                        <h4 class="text-color-dark font-weight-semibold">Suspendisse tincidunt nibh.</h4>
                        <p class="custom-text-color-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad cum eos quas, explicabo, possimus harum optio consequuntur quod veniam.</p>
                    </div>
                    <div class="custom-timeline-box">
                        <span class="date font-weight-bold text-color-primary">2017</span>
                        <h4 class="text-color-dark font-weight-semibold">Awards and 300 empolyees</h4>
                        <p class="custom-text-color-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, molestias numquam culpa, eligendi debitis dicta, hic delectus obcaecati accusantium quaerat magni explicabo et dolorem mollitia cumque sint deserunt. Labore vel magnam quasi.</p>
                    </div> --}}
                    <div class="timeline-bar"></div>
                </div>
            </div>
            <div class="col-lg-6 text-center">
                <img src="img/demos/finance/others/our-history-1.jpg" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</section>


@endsection
