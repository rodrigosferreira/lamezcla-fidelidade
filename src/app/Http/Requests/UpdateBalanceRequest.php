<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateBalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * 'sale_value', 'credits', 'debits', 'date'
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'balance' => 'required|numeric'
        ];
    }
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
          $balance = floatval(str_replace(',', '.', $this->request->all()['balance']));

          if ($balance < 0)
          {
            $validator->errors()->add('Novo Saldo', 'Valores não podem ser negativos');
          }
        });
    }
}
