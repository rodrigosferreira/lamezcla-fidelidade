<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt':0, 'stickySetTop': '0'}">
    <div class="header-body">
        <!-- /HEADER CONTAINER -->
        <div class="header-container container">
            <!-- HEADER ROW -->
            <div class="header-row">

                <!-- LOGO -->
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-logo">
                            <a href="/">
                                <img alt="LaMezcla" src="{{ asset('images/logo.svg') }}">
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /LOGO -->

                <!-- NAVIGATION  -->
                <div class="header-column justify-content-end">
                    <div class="header-row">
                        <div class="header-nav">
                            <div class="header-nav-main">
                                <nav class="collapse">
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li>
                                            <a class="nav-link @if (isRoute('home')) active @endif" href="{{ route('home') }}">
                                                Página Inicial
                                            </a>
                                        </li>
                                        <li>
                                            <a class="nav-link @if (Request::is('blog/*')) active @endif" href="#">
                                                Blog
                                            </a>
                                        </li>
                                        <li>
                                            <a class="nav-link @if (isRoute('about')) active @endif" href="{{ route('about') }}">
                                                Sobre
                                            </a>
                                        </li>
                                        <li>
                                            <a class="nav-link @if (isRoute('site.client.dashboard')) active @endif" href="{{ route('site.client.dashboard') }}">
                                                Área do Cliente
                                            </a>
                                        </li>
                                        <li>
                                            <a class="nav-link @if (isRoute('contact')) active @endif" href="{{ route('contact') }}">
                                                Contato
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /NAVIGATION -->

            </div>
            <!-- /HEADER ROW -->
        </div>
        <!-- /HEADER CONTAINER -->

        <!-- SOCIAL MEDIA -->
        <div class="wrapper-header-social-icons">
            <ul class="header-social-icons social-icons d-none d-sm-block">
                <li class="social-icons-instagram">
                    <a href="http://www.instagram.com/lamezclabr" target="_blank" title="Instagram">
                        <i class="fab fa-instagram"></i>
                    </a>
                </li>
                <li class="social-icons-facebook">
                    <a href="http://www.facebook.com/lamezclabr" target="_blank" title="Facebook">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- /SOCIAL MEDIA -->
    </div>      
</header>