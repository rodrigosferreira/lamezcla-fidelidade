<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *'name', 'cpf', 'email', 'phone', 'password', 'balance'
     * @return array
     */

    public function rules()
    {
      $rules = [
          'name' => 'required',
          'email' => 'required|unique:clients',
          'phone' => 'required|unique:clients',
          'password' => 'required|min:6|confirmed'
      ];

      if ($this->route('id') !== null) {
          $rules['password'] = 'nullable|min:6|confirmed';
      }

      return $rules;
    }
}
