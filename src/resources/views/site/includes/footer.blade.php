<footer id="footer" class="color color-secondary short">
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>
                        <i class="fas fa-map-marker-alt"></i> RUA MARIZ E BARROS, 1037 - MARACANÃ/RJ
                        <span class="separator">|</span> 
                        <i class="far fa-envelope"></i> <a href="mailto:contato@lamezcla.com.br">CONTATO@LAMEZCLA.COM.BR</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>