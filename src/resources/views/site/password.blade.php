@extends('layouts.site.auth')

@section('content')
    <div class="row">
        <div class="col-xs-12 login-container">
            <div id="login-box">
                {!! Form::open(['route' => 'site.password.change']) !!}

                @include('includes/_error_list')
                <div class="form-group">
                    {!! Form::label('new_password', 'Nova senha') !!}
                    {!! Form::password('new_password', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('new_password_confirmation', 'Confirmação') !!}
                    {!! Form::password('new_password_confirmation', ['class' => 'form-control']) !!}
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-right">
                            <button type="submit" class="button">
                                Trocar Senha
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

