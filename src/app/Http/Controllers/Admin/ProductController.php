<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Http\Requests\ProductRequest;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index() {
        $products = Product::all();
        return view('admin.product.index', ['products' => $products]);
    }

    public function create() {
        return view('admin.product.form');
    }

    public function store(ProductRequest $request) {
        Product::create($request->all());
        return redirect()->route('admin.product.index');
    }

    public function edit($id) {
        $product = Product::findOrFail($id);

        return view('admin.product.form', ['product' => $product]);
    }

    public function update(ProductRequest $request, $id) {
        $product = Product::findOrFail($id);
        $product->update($request->all());
        return redirect()->route('admin.product.index');
    }

    public function destroy($id) {
        Product::destroy($id);
        return redirect()->route('admin.product.index');
    }
}
