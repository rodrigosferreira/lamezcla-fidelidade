<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Client;
use App\Product;
use App\Sale;
use App\Http\Requests\SaleRequest;
use App\Http\Requests\PurchaseRequest;
use DB;

class ClientController extends Controller
{

    public function index () {
      return view('admin.client.index', ['clients' => Client::all()]);
    }

    public function manageSales($id) {
      $client = Client::findOrFail($id);
      $products = Product::all()->pluck('name', 'id');
      return view ('admin.client.manage_sales', ['client' => $client, 'products' => $products]);
    }

    public function managePurchases($id) {
        $client = Client::findOrFail($id);
        $products = Product::all()->pluck('name', 'id');
        return view ('admin.client.manage_purchases', ['client' => $client, 'products' => $products]);
      }

    public function storeSale(SaleRequest $request, $id) {
        $data = $request->all();
        $sales = [];
        $client = Client::find($id);

        foreach($data['products'] as $productData) {
            $product = Product::find($productData['product_id']);

            $sale = [
                'product_id' => $product->id,
                'client_id' => $client->id,
                'credited_points' => $product->points_on_purchase,
                'debited_points' => 0,
                'date' => $data['date']
            ];

            for ($i = 0; $i < $productData['quantity']; $i++) {
                array_push($sales, $sale);
            }
        }

        DB::transaction(function () use ($sales, $client) {
            foreach ($sales as $sale) {
                $client->points = $client->points + $sale['credited_points'];
                $client->save();
            }

            DB::table('sales')->insert($sales);
        });

        return redirect()->route('admin.client.manage_sales', ['id' => $client->id]);
    }

    public function storePurchase(PurchaseRequest $request, $id) {
        $data = $request->all();
        $product = Product::find($id);
        $client = Client::find($id);

        if ($client->points < $product->required_points) {
            return redirect()->back()->withInput()->withErrors("Este produto requer um saldo de pelo menos {$product->required_points} pontos para ser comprado.");
        }

        $sale = [
            'product_id' => $product->id,
            'client_id' => $client->id,
            'credited_points' => 0,
            'debited_points' => $product->required_points,
            'date' => $data['date']
        ];

        DB::transaction(function () use ($sale, $client) {
            $client->points = $client->points - $sale['debited_points'];
            $client->save();

            Sale::create($sale);
        });

        return redirect()->route('admin.client.manage_purchases', ['id' => $client->id]);
    }
}
