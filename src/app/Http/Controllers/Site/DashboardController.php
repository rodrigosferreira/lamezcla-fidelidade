<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index() {
        return view('site.dashboard', ['client' => Auth::guard('client')->user()]);
    }

    public function logout() {
        Auth::guard('client')->logout();

        return redirect('/');
    }
}
