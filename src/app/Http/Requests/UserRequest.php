<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
      $rules = [
          'name' => 'required',
          'email' => 'required|email',
          'cpf' => ['required', Rule::unique('users')->ignore($this->route('id')), 'digits:11'],
          'password' => 'required|min:6|confirmed',
          'authority' => 'required',
      ];

      if ($this->route('id') !== null) {
          $rules['password'] = 'nullable|min:6|confirmed';
      }

      return $rules;
    }
}
