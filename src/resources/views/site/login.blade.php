@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Entre com seus credenciais</h3>
                </div>
                <div class="panel-body">
                    @include('includes._error_list')
                    {{ Form::open(['route' => 'site.login']) }}
                        <div class="form-group">
                            {{ Form::label('phone', 'Celular:') }}
                            {{ Form::text('phone', null, ['class' => 'form-control']) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('password', 'Senha:') }}
                            {{ Form::password('password', ['class' => 'form-control']) }}
                        </div>

                        <p class="signup-info text-right">Ainda não é cadastrado? Clique <a href="{{ route('site.register') }}" class="signup-link">aqui</a></p>

                        <div class="pull-right">
                            <button type="submit" class="btn btn-success">
                                Acessar <i class="fa fa-sign-in fa-fw"></i>
                            </button>
                        </div>
                    {{ Form::close() }}    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
