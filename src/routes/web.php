<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::domain(env('APP_URL'))->any('/{any}', function() {
    if (isset(Route::current()->parameters['any'])) {
        return redirect('http://' . str_replace('http://', 'www.', Request::url()));
    } else {
        return redirect()->route('home');
    }
})->where('any', '.*');

// Client Authentication Routes...
Route::domain('www.' . env('APP_URL'))->group(function() {
    Route::get('/area-do-cliente/login', 'Auth\ClientLoginController@showLoginForm');
    Route::post('/area-do-cliente/login', 'Auth\ClientLoginController@login')->name('site.login');
    Route::get('/area-do-cliente/cadastro', function() {
        return view('site.register');
    })->name('site.register');
    Route::post('/area-do-cliente/cadastro', 'Site\ClientController@store')->name('site.client.store');
});

// Admin Authentication Routes...
Route::domain('equipe.' . env('APP_URL'))->group(function() {
    Route::get('/login', 'Auth\LoginController@showLoginForm');
    Route::post('/login', 'Auth\LoginController@login')->name('admin.login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
});

// Website Routes...
Route::domain('www.' . env('APP_URL'))->group(function() {
    Route::get('/', function () {
        return view('site.home'); 
    })->name('home');

    Route::get('/blog/inauguracao-lamezcla', function() {
        return view ('site.blog.inauguracao-lamezcla');
    })->name('blog.inauguracao-lamezcla');
    Route::get('/blog/america-latina', function() {
        return view ('site.blog.america-latina');
    })->name('blog.america-latina');

    Route::get('/contato', function() {
       return view('site.contact');
    })->name('contact');

    Route::get('/sobre-nos', function() {
        return view('site.about');
    })->name('about');

});

// Client Area Routes
Route::domain('www.' . env('APP_URL'))->middleware('auth:client')->group(function() {
    Route::get('/area-do-cliente', function () {
        return redirect()->route('site.client.dashboard');
    });

    Route::get('/area-do-cliente/dashboard', 'Site\DashboardController@index')->name('site.client.dashboard');
    Route::get('/area-do-cliente/logout', 'Site\DashboardController@logout')->name('site.client.logout');
});

// Admin Dashboard Routes...
Route::domain('equipe.' . env('APP_URL'))->middleware('auth')->namespace('Admin')->group(function() {
    Route::get('/', function() {
        return redirect()->route('admin.dashboard');
    });

    Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');

    Route::get('/area-do-cliente', function () {
        return redirect()->route('admin.dashboard');
    });

    Route::get('clientes', 'ClientController@index')->name('admin.client.index');
    Route::get('clientes/{id}/vendas', 'ClientController@manageSales')->name('admin.client.manage_sales');
    Route::get('clientes/{id}/compras', 'ClientController@managePurchases')->name('admin.client.manage_purchases');
    // Route::get('clientes/{id}/alterar_saldo', 'ClientController@updateBalance')->name('admin.client.update_balance');
    // Route::post('clientes/{id}/novosaldo', 'ClientController@storeBalance')->name('admin.client.store_balance');
    Route::post('clientes/{id}/venda', 'ClientController@storeSale')->name('admin.client.store_sale');
    Route::post('clientes/{id}/compra', 'ClientController@storePurchase')->name('admin.client.store_purchase');
    // Route::post('clientes/{id}/resetar_senha', 'ClientController@passwordReset')->name('admin.client.password_reset');

    Route::get('usuarios', 'UserController@index')->name('admin.user.index');
    Route::get('usuarios/cadastrar', 'UserController@create')->name('admin.user.create');
    Route::post('usuarios/cadastrar', 'UserController@store')->name('admin.user.store');
    Route::get('usuarios/{id}/editar', 'UserController@edit')->name('admin.user.edit');
    Route::put('usuarios/{id}/editar', 'UserController@update')->name('admin.user.update');
    Route::get('/usuarios/{id}/deletar', 'UserController@destroy')->name('admin.user.destroy');

    // Products
    Route::get('/produtos', 'ProductController@index')->name('admin.product.index');
    Route::get('/produtos/cadastrar', 'ProductController@create')->name('admin.product.create');
    Route::post('/produtos/cadastrar', 'ProductController@store')->name('admin.product.store');
    Route::get('/produtos/{id}/editar', 'ProductController@edit')->name('admin.product.edit');
    Route::put('/produtos/{id}/editar', 'ProductController@update')->name('admin.product.update');
    Route::get('/produtos/{id}/deletar', 'ProductController@destroy')->name('admin.product.destroy');

    Route::get('/vendas-sem-cadastro', 'UnregisteredSaleController@index')->name('admin.unregistered-sale.index');
    Route::post('/vendas-sem-cadastro', 'UnregisteredSaleController@store')->name('admin.unregistered-sale.store');
});

// Registration Routes...
//$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
//$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
//$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
//$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
//$this->post('password/reset', 'Auth\ResetPasswordController@reset');

// Route::domain('admin.' . env('APP_URL'))->group(['namespace' => 'Admin', 'middleware' => 'auth'], function() {
//     Route::get('/', function() {
//         redirect()->route('admin.dashboard');
//     });

//     Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');

//     Route::get('/area-do-cliente', function () {
//         return redirect()->route('admin.dashboard');
//     });

//     Route::get('clientes', 'ClientController@index')->name('admin.client.index');
//     Route::get('clientes/{id}', 'ClientController@manage')->name('admin.client.manage');
//     Route::get('clientes/{id}/alterar_saldo', 'ClientController@updateBalance')->name('admin.client.update_balance');
//     Route::post('clientes/{id}/novosaldo', 'ClientController@storeBalance')->name('admin.client.store_balance');
//     Route::post('clientes/{id}/venda', 'ClientController@storeSale')->name('admin.client.store_sale');
//     Route::post('clientes/{id}/resetar_senha', 'ClientController@passwordReset')->name('admin.client.password_reset');

//     Route::get('usuarios', 'UserController@index')->name('admin.user.index');
//     Route::get('usuarios/cadastrar', 'UserController@create')->name('admin.user.create');
//     Route::post('usuarios/cadastrar', 'UserController@store')->name('admin.user.store');
//     Route::get('usuarios/{id}/editar', 'UserController@edit')->name('admin.user.edit');
//     Route::put('usuarios/{id}/editar', 'UserController@update')->name('admin.user.update');
//     Route::get('/usuarios/{id}/deletar', 'UserController@destroy')->name('admin.user.destroy');

//     Route::get('creditos/cpf', 'WaitingListController@index')->name('admin.waiting_list.index');
//     Route::post('creditos/cpf/salvar', 'WaitingListController@store')->name('admin.waiting_list.store');
// });

// Route::domain(env('APP_URL'))->group(['namespace' => 'Site', 'middleware' => 'auth:client'], function() {
//     Route::group(['middleware' => 'checkPasswordChange'], function() {
//         Route::get('/area-do-cliente', 'DashboardController@index');
//         Route::get('/dashboard', 'DashboardController@index')->name('site.dashboard');
//     });

//     Route::get('/trocar_senha', 'PasswordController@index')->name('site.password');
//     Route::post('/trocar_senha', 'PasswordController@change')->name('site.password.change');
//     Route::get('/logout', 'DashboardController@logout')->name('site.logout');
// });

// Route::get('/cadastrar', 'Site\ClientController@create')->name('site.client.create');
// Route::post('/cadastrar', 'Site\ClientController@store')->name('site.client.store');
// Route::get('/regras', function() {
//    return view('site.rules');
// })->name('site.rules');
