@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Entre com seus credenciais</h3>
                </div>
                <div class="panel-body">
                    @include('includes._error_list')
                    {{ Form::open(['route' => 'admin.login']) }}
                        <div class="form-group">
                            {{ Form::label('email', 'E-mail:') }}
                            {{ Form::text('email', null, ['class' => 'form-control']) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('password', 'Senha:') }}
                            {{ Form::password('password', ['class' => 'form-control']) }}
                        </div>

                        <button type="submit" class="btn btn-success btn-block">Acessar</button>
                    {{ Form::close() }}    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
