@extends('layouts.admin')

@section('title', 'Vendas para clientes sem cadastro')

@section('content')

<div class="row">
  <div class="col-lg-12">
        <div class="panel panel-default">
                <div class="panel-heading">Formulário de venda</div>
                {!! Form::open(['method' => 'post', 'route' => ['admin.unregistered-sale.store'], 'class' => 'repeater']) !!}
                <div class="panel-body">
                    @include('includes/_error_list')
                    <div class="form-group">
                        {!! Form::label('date', 'Data da  Compra') !!}
                        {!! Form::date('date', \Carbon\Carbon::now()) !!}
                    </div>
                    <h3>Produtos</h3>
                    <hr>
                    <div data-repeater-list="products">
                        <div class="form-group" data-repeater-item>
                            <div class="row" >
                                <div class="col-lg-8 col-md-7 col-xs-6">
                                    {{ Form::select('product_id', $products, null, ['class' => 'form-control', 'placeholder' => 'Selecione um produto']) }}
                                </div>
                                <div class="col-lg-3 col-md-4 col-xs-4">
                                    {{ Form::number('quantity', null, ['class' => 'form-control']) }}
                                </div>
                                <div class="col-lg-1 col-md-1 col-xs-2">
                                    <a href="#" data-repeater-delete class="btn btn-danger pull-right"><i class="fa fa-times"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <a href="#" data-repeater-create class="btn btn-success"><i class="fa fa-plus"></i> </a>
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone', 'Celular') !!}
                        {!! Form::text('phone', null, ['class' => 'form-control phone']) !!}
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success">Registrar Venda</button>
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Vendas para clientes ainda não cadastrados
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>Data</th>
                        <th>Produto</th>
                        <th>Celular do Cliente</th>
                        <th>Pontos</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($contents as $content)
                        <tr>
                            <td>{{ $content->date }}</td>
                            <td>{{ $content->product->name }}</td>
                            <td>{{ $content->phone }} </td>
                            <td>{{ $content->credited_points > 0 ? $content->credited_points : "-$content->debited_points" }} </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
        </div>
    </div>
</div>
@endsection
