@extends('layouts.admin')

@section('title', 'Usuários')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista de Usuários
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover table-responsive datatable">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Tipo</th>
                            <th>Opções</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->name }} </td>
                                <td>{{ $user->email }}</td>
                                <td><span class="label label-default">{{ get_user_type($user->authority) }}</span></td>
                                <td>
                                    <div class="bottom-actions">
                                        <a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-primary">Editar</a>
                                        <a href="{{ route('admin.user.destroy', $user->id) }}" class="btn btn-danger">Excluir</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                    <div class="panel-footer">
                    <a href="{{ route('admin.user.create') }}" class="btn btn-success">Adicionar novo usuário</a>
                </div>
            </div>
        </div>
    </div>
@endsection
