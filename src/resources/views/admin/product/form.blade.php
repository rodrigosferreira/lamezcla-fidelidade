@extends('layouts.admin')

@section('title', 'Produtos')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if (isset($product))
                        Atualização de dados do Produto
                    @else
                        Cadastro de Produto
                    @endif
                </div>
                @if (isset($product))
                    {!! Form::model($product, ['route' => ['admin.product.update', $product->id], 'method' => 'PUT']) !!}
                @else
                    {!! Form::open() !!}
                @endif
                <div class="panel-body">
                  @include('includes/_error_list')
                    <div class="form-group">
                        {!! Form::label('name', 'Nome') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('required_points', 'Pontos Requeridos') !!}
                        {!! Form::text('required_points', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('points_on_purchase', 'Pontos na Compra') !!}
                        {!! Form::text('points_on_purchase', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="{{ route('admin.product.index') }}" class="btn btn-default">Cancelar</a>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success">
                                    @if (isset($product))
                                        Atualizar Dados
                                    @else
                                        Cadastrar Produto
                                    @endif
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
@endsection
