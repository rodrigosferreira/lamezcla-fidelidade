<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnregisteredSale extends Model
{
  protected $fillable = [
    'product_id', 'phone', 'credited_points', 'date'
  ];

  protected $dates = [
    'date'
  ];

  public $timestamps = false;

  public function product() {
    return $this->belongsTo(Product::class);
  }
}
