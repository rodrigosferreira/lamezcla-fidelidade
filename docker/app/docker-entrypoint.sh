#!/bin/bash
cd ..

if [ ! "$(ls -A html)" ]
then
  composer create-project laravel/laravel html
  rm html/.env
fi

cd html

composer install

if [ ! -e .env ]
then
  envsubst < /home/lamezcla/.env.example > .env.example
  cp .env.example .env
  php artisan key:generate
fi

php artisan migrate

if [ -e storage ]
then
    chmod -R 775 storage/
fi

php-fpm
