<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Client;

class UnregisteredSaleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
      $rules = [
          'phone' => 'required',
          'products.*.product_id' => 'required',
          'products.*.quantity' => 'required|integer|min:1',
          'date' => 'required'
      ];


      return $rules;
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
          $client = Client::where('phone', $this->request->all()['phone'])->first();

          if ($client != null){
            $validator->errors()->add('phone', 'Cliente Já cadastrado no sistema');
          }
        });
    }
}
