<?php

namespace App\Http\Controllers\Site;

use App\Http\Requests\ClientPasswordRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PasswordController extends Controller
{
    public function index() {
        return view('site.password');
    }

    public function change(ClientPasswordRequest $request) {
        $client = auth('client')->user();
        $client->password = bcrypt($request->get('new_password'));
        $client->change_password = false;
        $client->save();

        return redirect()->route('site.dashboard');
    }
}
