@extends('layouts.admin')

@section('title', 'Produtos')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista de Produtos
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover table-responsive datatable">
                        <thead>
                        <tr>
                            <th>Produto</th>
                            <th>Pontos Requeridos</th>
                            <th>Pontos na Compra</th>
                            <th>Opções</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <td>{{ $product->name }} </td>
                                <td>{{ $product->required_points }}</td>
                                <td>{{ $product->points_on_purchase }}</td>
                                <td>
                                    <div class="bottom-actions">
                                        <a href="{{ route('admin.product.edit', $product->id) }}" class="btn btn-primary">Editar</a>
                                        <a href="{{ route('admin.product.destroy', $product->id) }}" class="btn btn-danger">Excluir</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                    <div class="panel-footer">
                    <a href="{{ route('admin.product.create') }}" class="btn btn-success">Adicionar novo produto</a>
                </div>
            </div>
        </div>
    </div>
@endsection
