<div class="slider-container rev_slider_wrapper" style="height: 650px; margin-left: 10px;">
    <div id="revolutionSlider" class="slider rev_slider" data-version="5.4.7" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1920, 'gridheight': 650, 'disableProgressBar': 'on'}">
        <ul>
            <!-- Slide 01 -->
            <li data-transition="fade">
                <img src="/images/home-slider/slide1.png"
                    alt=""
                    data-bgposition="center center"
                    data-bgfit="cover"
                    data-bgrepeat="no-repeat"
                    class="rev-slidebg">

                <div class="tp-caption top-label alternative-font hideOnMobile"
                    data-x="left" data-hoffset="25"
                    data-y="center" data-voffset="-55"
                    data-start="500"
                    style="z-index: 5"
                    data-transform_in="y:[-300%];opacity:0;s:500;">BEM VINDOS AO</div>

                <div class="tp-caption main-label"
                    data-x="left" data-hoffset="25"
                    data-y="center" data-voffset="-5"
                    data-start="1500"
                    data-whitespace="nowrap"
                    data-transform_in="y:[100%];s:500;"
                    data-transform_out="opacity:0;s:500;"
                    style="z-index: 5"
                    data-mask_in="x:0px;y:0px;">LA MEZCLA</div>

                <div class="tp-caption bottom-label hideOnMobile"
                    data-x="left" data-hoffset="25"
                    data-y="center" data-voffset="40"
                    data-start="2000"
                    style="z-index: 5; font-size: 1.8em;"
                    data-transform_in="y:[100%];opacity:0;s:500;">Venha experimentar a comida Latino Americana reinventada!</div>

                <a class="tp-caption btn btn-md btn-primary hideOnMobile"
                    data-hash
                    data-hash-offset="85"
                    href="#menu"
                    data-x="left" data-hoffset="25"
                    data-y="center" data-voffset="85"
                    data-start="2200"
                    data-whitespace="nowrap"
                    data-transform_in="y:[100%];s:500;"
                    data-transform_out="opacity:0;s:500;"
                    style="z-index: 5"
                    data-mask_in="x:0px;y:0px;">Nosso Cardápio <i class="fa fa-long-arrow-alt-down"></i></a>

            </li>
            <!-- /Slide 01 -->
            <!-- Slide 02 -->
            <li data-transition="fade">
                <img src="/images/home-slider/slide2.png"
                    alt=""
                    data-bgposition="center center"
                    data-bgfit="cover"
                    data-bgrepeat="no-repeat"
                    class="rev-slidebg">

                <div class="tp-caption top-label alternative-font hideOnMobile"
                    data-x="left" data-hoffset="25"
                    data-y="center" data-voffset="-55"
                    data-start="500"
                    style="z-index: 5"
                    data-transform_in="y:[-300%];opacity:0;s:500;"></div>

                <div class="tp-caption main-label"
                    data-x="left" data-hoffset="25"
                    data-y="center" data-voffset="-5"
                    data-start="1500"
                    data-whitespace="nowrap"
                    data-transform_in="y:[100%];s:500;"
                    data-transform_out="opacity:0;s:500;"
                    style="z-index: 5"
                    data-mask_in="x:0px;y:0px;"></div>

                <div class="tp-caption bottom-label hideOnMobile"
                    data-x="left" data-hoffset="25"
                    data-y="center" data-voffset="40"
                    data-start="2000"
                    style="z-index: 5; font-size: 1.2em;"
                    data-transform_in="y:[100%];opacity:0;s:500;"></div>
            </li>
            <!-- /Slide 02 -->
        </ul>
    </div>
</div>
