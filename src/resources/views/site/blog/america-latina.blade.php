@extends('site.layouts.master')

@section('content')
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h1 class="mt-5 mb-2 pt-3">Blog La Mezcla</h1>
							<p class="text-3">Um pouco sobre nós! Esperamos que você possa nos conhecer melhor.<br> Traremos um pouco sobre a cultura latino americana e os principais aspectos de nossa cozinha, além de aproximá-los para o nosso dia-a-dia! Sejam bem vindos!</p>

							<hr class="custom-divider">
						</div>
					</div>
				</div>

				<div class="container">

					<div class="row pt-2">
						<div class="col">
							<div class="blog-posts single-post mt-5">

								<article class="post post-large blog-single-post">

									<div class="post-date">
										<span class="day">26</span>
										<span class="month">Abril</span>
									</div>

									<div class="post-content">

										<h1 class="mb-3">América Latina</h1>

										<div class="post-meta">
											<span><i class="fas fa-user"></i> Por <a href="#">Rodrigo Ferreira</a> </span>
											<span><i class="fas fa-tag"></i> <a href="#">Países Latino Americanos</a>, <a href="#">Curiosidades</a> </span>
										</div>

										<img src="/images/america-latina.png" class="img-fluid float-right mb-3 mb-1 ml-4" alt="" style="width: 330px;">

										<p class="lead">A mistura está.. nos países! A América Latina é composta, em sua essência, pelos países das Américas que foram colonizados por europeus cuja língua é derivada do latim (principalmente espanhol, português e francês)! Sendo assim, temos um total de 20 países que fazem parte da América Latina. Em ordem alfabética:</p>

										<p>Argentina,Bolívia, Brasil, Chile, Colômbia, Costa Rica, Cuba, Equador, El Salvador, Guatemala, Haiti, Honduras, México, Nicarágua, Panamá, Paraguai, Peru, República Dominicana, Uruguai e Venezuela.</p>

										<p>Nossa inauguração está chegando! Gostaríamos de convidar a todos os nossos clientes, amigos e família para conhecer o La Mezcla — Cozinha Autoral Latino Americana, que ocorrerá no dia 01º de Maio de 2018 (Dia do Trabalhador, feriado.. coincidência?)</p>

										<p>Observe que os EUA e Canadá, países da América do Norte, não fazem parte da América Latina, já que não foram colonizados por países românicos.</p>

										<p>A América Latina é multicultural! Todos os países possuem características próprias, seja na música, na dança, na arte e também na gastronomia! Exploraremos com muito carinho nos próximos meses a gastronomia local, também trazendo um pouco da cultura de cada um dos países que nos inspiram!</p>

										<p>Acompanhe o nosso blog com postagens semanais sobre cada um dos países e se delicie com nossos pratos!</p>

										<p>Equipe La Mezcla</p>

										<div class="post-block post-author mt-5 clearfix">
											<h4 class="mt-5 mb-3">Autor</h4>
											<div class="img-thumbnail d-block">
												<a href="#">
													<img src="/images/perfis/profile-rodrigo.jpg" alt="">
												</a>
											</div>
											<p><strong class="name mb-3">Rodrigo Ferreira</strong></p>
											<p class="mt-1"></p>
										</div>
									</div>
								</article>

							</div>
						</div>
					</div>

				</div>
		</div>
@endsection
