<?php

function get_user_type($authority) {
    switch($authority) {
        case 0:
            return 'Caixa';
            break;
        case 100:
            return 'Gerente';
            break;
        default:
            return 'Unknown';
    }
}

function isRoute($name) {
    $path = route($name, [], false);

    if ($path !== '/') {
        $path = trim($path, '/');
    }

    return Request::is($path);
}