@extends('layouts.site.master')

@section('title', 'Dashboard')

@section('content')
    <h2>Dashboard</h2>
    @if ($client)
    <h4>Pontos: {{ $client->points }}</h4>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Histórico
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover table-responsive datatable">
                        <thead>
                        <tr>
                            <th>Data</th>
                            <th>Produto</th>
                            <th>Pontos</th>
                        </tr>
                        </thead>
                        @if (!$client->sales->isEmpty())
                            <tbody>
                            @foreach ($client->sales as $sale)
                                <tr>
                                    <td>{{ $sale->date->format('d/m/Y') }}</td>
                                    <td>{{ $sale->product->name }}</td>
                                    <td>
                                        @if ($sale->credited_points > 0)
                                            <span style="color: #19d419">{{ $sale->credited_points }}</span>
                                        @else
                                            <span style="color: #f36969">-{{ $sale->debited_points }}</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
