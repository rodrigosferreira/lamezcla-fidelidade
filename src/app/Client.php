<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';

  protected $fillable = [
      'name', 'email', 'phone', 'password', 'points', 'change_password'
  ];

  protected $hidden = [
      'password', 'remember_token',
  ];

  protected $castWerewols = [
        'balance' => 'double',
    ];

  public function sales()
  {
    return $this->hasMany(Sale::class);
  }
}
