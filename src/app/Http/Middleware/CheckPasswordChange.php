<?php

namespace App\Http\Middleware;

use Closure;

class CheckPasswordChange
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->change_password) {
            return redirect()->route('site.password');
        }

        return $next($request);
    }
}
