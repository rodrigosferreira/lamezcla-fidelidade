@extends('layouts.site.auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Entre com suas informações</h3>
                </div>
                <div class="panel-body">
                    @include('includes._error_list')
                    {!! Form::open(['route' => 'site.client.store']) !!}
                    <div class="form-group">
                        {!! Form::label('name', 'Nome') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone', 'Celular') !!}
                        {!! Form::text('phone', null, ['class' => 'form-control phone']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', 'E-mail') !!}
                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('password', 'Senha') !!}
                        {!! Form::password('password', ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('password_confirmation', 'Confirmação de senha') !!}
                        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <a href="{{ route('site.login') }}" class="btn btn-default">Voltar</a>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success">
                                    Cadastrar
                                </button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!} 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

