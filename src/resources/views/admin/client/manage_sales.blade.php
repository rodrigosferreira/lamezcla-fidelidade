@extends('layouts.admin')

@section('title', 'Cliente - ' . $client->name)

@section('content')
<h3>Pontos: {{ $client->points }}</h3>

<div class="panel panel-default">
    <div class="panel-heading">Formulário de venda</div>
    {!! Form::open(['method' => 'post', 'route' => ['admin.client.store_sale', $client->id], 'class' => 'repeater']) !!}
        <div class="panel-body">
            @include('includes/_error_list')
            <div class="form-group">
                {!! Form::label('date', 'Data da  Compra') !!}
                {!! Form::date('date', \Carbon\Carbon::now()) !!}
            </div>
            <h3>Produtos</h3>
            <hr>
            <div data-repeater-list="products">
                <div class="form-group" data-repeater-item>
                    <div class="row" >
                        <div class="col-lg-8 col-md-7 col-xs-6">
                            {{ Form::select('product_id', $products, null, ['class' => 'form-control', 'placeholder' => 'Selecione um produto']) }}
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-4">
                            {{ Form::number('quantity', null, ['class' => 'form-control']) }}
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2">
                            <a href="#" data-repeater-delete class="btn btn-danger pull-right"><i class="fa fa-times"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <a href="#" data-repeater-create class="btn btn-success"><i class="fa fa-plus"></i> </a>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-xs-12">
                    <a href="{{ route('admin.client.index') }}" class="btn btn-default">Cancelar</a>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">Registrar Venda</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        Extrato
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <table class="table table-striped table-bordered table-hover table-responsive datatable">
            <thead>
            <tr>
                <th>Data</th>
                <th>Produto</th>
                <th>Pontos</th>
            </tr>
            </thead>
            @if (!$client->sales->isEmpty())
                <tbody>
                @foreach ($client->sales as $sale)
                    <tr>
                        <td>{{ $sale->date->format('d/m/Y') }}</td>
                        <td>{{ $sale->product->name }}</td>
                        <td>
                            @if ($sale->credited_points > 0)
                                <span style="color: #19d419">{{ $sale->credited_points }}</span>
                            @else
                                <span style="color: #f36969">-{{ $sale->debited_points }}</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            @endif
        </table>
    </div>
</div>

@endsection
