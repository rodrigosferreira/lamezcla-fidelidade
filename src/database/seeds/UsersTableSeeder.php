<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Alan Rocha',
                'email' => 'alan@webskills.com.br',
                'authority' => 100,
                'password' => bcrypt('@webskills#')
            ],
            [
                'name' => 'Rodrigo Ferreira',
                'email' => 'rodrigo@webskills.com.br',
                'authority' => 100,
                'password' => bcrypt('@webskills#')
            ]
        ];
        
        foreach ($users as $user) {
            User::create($user);
        }
    }
}
