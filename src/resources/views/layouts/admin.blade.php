<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ asset('css/core.css') }}">
    <link rel="stylesheet" href="{{ asset('js/vendor/metisMenu/metisMenu.min.css') }}">

    <!-- DataTables CSS -->
    <link href="{{ asset('js/vendor/datatables-plugins/dataTables.bootstrap.css') }}" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="{{ asset('js/vendor/datatables-responsive/dataTables.responsive.css') }}" rel="stylesheet">

    <!-- Sweet Alert -->
    <link rel="stylesheet" href="{{ asset('js/vendor/sweetalert/sweetalert.css') }}">

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>

<body>

<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">{{ config('app.name') }}</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> {{ Auth::guard('web')->user()->name }} ({{ get_user_type(Auth::guard('web')->user()->authority) }}) <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            Sair <i class="fa fa-power-off fa-fw"></i>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        <a href="{{ route('admin.client.index') }}"><i class="fa fa-users fa-fw"></i> Clientes</a>
                        @if (auth('web')->user()->authority >= 100)
                        <a href="{{ route('admin.user.index') }}"><i class="fa fa-user-circle fa-fw"></i> Usuários</a>
                        @endif
                        <a href="{{ route('admin.product.index') }}"><i class="fa fa-cubes fa-fw"></i> Produtos</a>
                        <a href="{{ route('admin.unregistered-sale.index') }}"><i class="fa fa-credit-card fa-fw"></i> Vendas sem cadastro</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('title')</h1>
                    @yield('content')
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<script src="{{ asset('js/core.js') }}"></script>

<!-- DataTables JavaScript -->
<script src="{{ asset('js/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{ asset('js/vendor/metisMenu/metisMenu.min.js') }}"></script>

<!-- Sweet Alert -->
<script src="{{ asset('js/vendor/sweetalert/sweetalert.min.js') }}"></script>

<!-- Jquery Repeater -->
<script src="{{ asset('js/vendor/jquery.repeater/jquery.repeater.min.js') }}"></script>

<!-- Jquery InputMask -->
<script src="{{ asset('js/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>

<!-- Custom Theme JavaScript -->
<script src="{{ asset('js/app.js') }}"></script>

</body>

</html>
