@extends('site.layouts.master')

@section('content')
<!-- REV SLIDER -->
@include('site.includes.home-slider')
<!-- /REV Slider -->

<section class="pt-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="mt-4 mb-2">Junte-se a nós para uma <strong>Experiência Latino Americana!</strong></h2>
                <p class="text-3">Venha conhecer o La Mezcla! Aqui, o objetivo é trazer o sabor<br> da comida latino americana para perto de você!</p>

                <hr class="custom-divider">
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-md-4 pb-5">

                <div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="0">
                    <div class="thumb-info thumb-info-no-zoom thumb-info-custom mb-5 text-center">
                        <div class="thumb-info-side-image-wrapper p-0">
                        <img src="/images/sazonal.png" class="img-fluid" alt="">
                        </div>
                        <div class="thumb-info-caption">
                            <div class="thumb-info-caption-text">
                                <h2 class="mb-3 mt-1">Pratos Sazonais</h2>
                                <p class="text-3">Cardápio sempre atualizado! Conheça, ao longo das estações, os diferentes pratos latino americanos disponíveis!</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-4 pb-5">

                <div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="300">
                    <div class="thumb-info thumb-info-no-zoom thumb-info-custom mb-5 text-center">
                        <div class="thumb-info-side-image-wrapper p-0">
                            <img src="/images/autoral.png" class="img-fluid" alt="">
                        </div>
                        <div class="thumb-info-caption">
                            <div class="thumb-info-caption-text">
                                <h2 class="mb-3 mt-1">Cozinha Autoral</h2>
                                <p class="text-3">São opções conhecidas dos países, reformulada de acordo com as experiências de nosso chef! Aproveite!</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-4 pb-5">

                <div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="600">
                    <div class="thumb-info thumb-info-no-zoom thumb-info-custom mb-5 text-center">
                        <div class="thumb-info-side-image-wrapper p-0">
                            <img src="/images/fidelidade.png" class="img-fluid" alt="">
                        </div>
                        <div class="thumb-info-caption">
                            <div class="thumb-info-caption-text">
                                <h2 class="mb-3 mt-1">Fidelidade</h2>
                                <p class="text-3">Seja nosso cliente fiel e ganhe descontos para as próximas compras! Acesse a nossa área de cliente e faça seu cadastro!</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

{{-- <section class="section section-background section-center" style="background-image: url(img/demos/restaurant/parallax-restaurant.jpg); background-position: 50% 100%; min-height: 615px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <hr class="custom-divider">
                <div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options="{'items': 1, 'loop': false}">
                    <div>
                        <div class="col">
                            <div class="testimonial testimonial-style-2 testimonial-with-quotes mb-0">
                                <blockquote>
                                    <p>The best place in town! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta, tincidunt turpis at, interdum tortor. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce ante tellus, convallis non consectetur sed, pharetra nec ex.</p>
                                </blockquote>
                                <div class="testimonial-author">
                                    <p><strong>John Smith</strong><span>Porto Magazine</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="col">
                            <div class="testimonial testimonial-style-2 testimonial-with-quotes mb-0">
                                <blockquote>
                                    <p>Best place to eat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta, tincidunt turpis at, interdum tortor. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </blockquote>
                                <div class="testimonial-author">
                                    <p><strong>John Smith</strong><span>Porto Magazine</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8">

            </div>
        </div>
    </div>
</section> --}}

<section class="pt-3 pb-3">
    <div class="container">
        <div class="row mb-5">
            <div class="col-lg-12 text-center">
                <h4 class="mt-4 mb-2">Nossa <strong>Galeria</strong></h4>
                <p><br>Veja um pouco do que estamos oferecendo!</p>

                <hr class="custom-divider">

                <div class="lightbox" data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}}">
                    <div class="masonry-loader masonry-loader-showing">
                        <div class="masonry" data-plugin-masonry data-plugin-options="{'itemSelector': '.masonry-item'}">
                            <div class="masonry-item">
                                <span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
                                    <span class="thumb-info-wrapper">
                                        <img src="images/gallery/choripan.png" class="img-fluid" alt="">
                                        <span class="thumb-info-action thumb-info-action-custom">
                                            <a href="images/gallery/choripan.png">
                                                <span class="thumb-info-icon-custom"></span>
                                            </a>
                                        </span>
                                    </span>
                                </span>
                            </div>
                            <div class="masonry-item w2">
                                <span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
                                    <span class="thumb-info-wrapper">
                                        <img src="images/gallery/chivito.png" class="img-fluid" alt="">
                                        <span class="thumb-info-action thumb-info-action-custom">
                                            <a href="images/gallery/chivito.png">
                                                <span class="thumb-info-icon-custom"></span>
                                            </a>
                                        </span>
                                    </span>
                                </span>
                            </div>
                            <div class="masonry-item">
                                <span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
                                    <span class="thumb-info-wrapper">
                                        <img src="images/gallery/completo.png" class="img-fluid" alt="">
                                        <span class="thumb-info-action thumb-info-action-custom">
                                            <a href="images/gallery/completo.png">
                                                <span class="thumb-info-icon-custom"></span>
                                            </a>
                                        </span>
                                    </span>
                                </span>
                            </div>
                            <div class="masonry-item">
                                <span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
                                    <span class="thumb-info-wrapper">
                                        <img src="images/gallery/cubano.png" class="img-fluid" alt="">
                                        <span class="thumb-info-action thumb-info-action-custom">
                                            <a href="images/gallery/cubano.png">
                                                <span class="thumb-info-icon-custom"></span>
                                            </a>
                                        </span>
                                    </span>
                                </span>
                            </div>
                            <div class="masonry-item">
                                <span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
                                    <span class="thumb-info-wrapper">
                                        <img src="images/gallery/ceviche.png" class="img-fluid" alt="">
                                        <span class="thumb-info-action thumb-info-action-custom">
                                            <a href="images/gallery/ceviche.png">
                                                <span class="thumb-info-icon-custom"></span>
                                            </a>
                                        </span>
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid">
    <div class="row mt-5">

        <div class="col-lg-12 p-0">
            <section class="section section-quaternary section-no-border h-100 mt-0">
                <div class="row justify-content-end">
                    <div class="col-half-section">
                        <div class="text-center">
                            <h4 class="mt-3 mb-0 heading-dark">Nosso <strong>Blog</strong></h4>
                            <p class="mb-1">Conheça um pouco sobre nós e de onde vem nossas inspirações!</p>

                            <hr class="custom-divider m-0">
                        </div>

                        <div class="owl-carousel owl-theme show-nav-title mt-5 mb-0" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'autoplay': true, 'autoplayTimeout': 5000, 'touchDrag': false, 'mouseDrag': false}">
                            <div>
                                <div class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom mb-3">
                                    <div class="thumb-info-side-image-wrapper p-0">
                                        <img src="/images/inauguracao.png" class="img-fluid" alt="" style="width: 165px;">
                                    </div>
                                    <div class="thumb-info-caption">
                                        <div class="thumb-info-caption-text">
                                            <h4 class="mb-0 mt-1 heading-dark">Inauguração do La Mezcla</h4>
                                            <p class="text-3 pt-3 pb-1">O dia está próximo.. um projeto a ser inaugurado com meses de suor e dedicação para que...</p>
                                        <a class="mt-2" href="{{route('blog.inauguracao-lamezcla')}}">Leia Mais <i class="fas fa-long-arrow-alt-right"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom">
                                    <div class="thumb-info-side-image-wrapper p-0">
                                        <img src="images/america-latina.png" class="img-fluid" alt="" style="width: 165px;">
                                    </div>
                                    <div class="thumb-info-caption">
                                        <div class="thumb-info-caption-text">
                                            <h4 class="mb-0 mt-1 heading-dark">A América Latina</h4>
                                            <p class="text-3 pt-3 pb-1">A mistura está… nos países! A América Latina é composta, em sua essência, pelos países...</p>
                                            <a class="mt-2" href="{{route('blog.america-latina')}}">Leia Mais <i class="fas fa-long-arrow-alt-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- CARDAPIO -->
<section id="menu" style="background-image: url(img/demos/restaurant/background-restaurant.png); background-position: 50% 100%; background-repeat: no-repeat;">
    <div class="container">
        <div class="row mt-3">
            <div class="col-lg-12 text-center">
                <h4 class="mt-4 mb-2">Nosso <strong>Cardápio</strong></h4>
                <p>Inspirado na gastronomia latino americana</p>

                <hr class="custom-divider">

                @include('site.includes.menu')
            </div>
        </div>

        {{-- <div class="row mb-0 mt-5">
            <div class="col-lg-12 text-center">
                 <a href="#" class="btn btn-primary btn-lg mb-5">Cardápio Completo</a>
            </div>
        </div>--}}
    </div>
</section>
<!-- /CARDAPIO -->
@endsection
