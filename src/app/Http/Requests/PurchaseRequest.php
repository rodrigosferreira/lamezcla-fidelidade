<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Client;

class PurchaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * 'sale_value', 'credits', 'debits', 'date'
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required',
            'date' => 'required'
        ];
    }
}
